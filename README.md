# NHL.py

--- As of the 2023-2024 season, the NHL has a new API. This no longer works ---

A python interface for the NHL's events API.

As of yet still very very in progress.
You're welcome to try and use it, but it'll just throw a lot of errors where pieces are wholesale missing.

## Thanks To

* Drew Hynes for his [repo](https://gitlab.com/dword4/nhlapi/) documenting the NHL's API
* [Tariq Daouda](http://www.tariqdaouda.com/) who wrote [pyArango](https://github.com/ArangoDB-Community/pyArango) which taught me a lot when I read through it 

## About

* Written with python 3.6.7

Ever looked at the NHL's [stats page](https://www.nhl.com/stats) and wondered what else you could do with those numbers?
Ever been discouraged at how much dreck you would need to deal with to scrape those values?
Well, despair no more! The NHL has a stats API running, and it has the basis for all these numbers.
You can even use them to calculate your own stats or explore the world of the NHL.

Unfortunately, there's a catch - it's undocumented.
Your only option was to go to Drew Hynes' [documentation repo](https://gitlab.com/dword4/nhlapi) and build your own HTTPS requests for specific data.
It was long and unweildy and full of KeyErrors where you forgot one layer of JSON or misspelled something. E.G.

    home = data['gameData']['teams']['home']['triCode']
    away = data['gameData']['teams']['away']['triCode']
    goals = data['liveData']['plays']['scoringPlays']
    goal = data['liveData']['plays']['allPlays'][goals[1]]

As somebody with two projects using this API, I dreaded going back into it and figuring everything out.
The solution, therefore, was to create a module to pull and organize the data without having to leave python.
And to document it so others could do the same.

The goal here is to create an interface to the stats API that lets you spend your time doing something interesting while this pulls data from the NHL and organizes it for you.

There isn't a whole lot of interesting things it does at the moment - its main purpose is to pull and organize the info to make other interesting things easier.

## To Do

* Complete tests for person.py (just splits to go)
* Write tests for game.py
* Write tests for team.py
* Write tests for schedule.py
* Create automatic tests (for now and always)
    1. Flask docker to serve pre-saved json
    2. Client docker with "statsapi.web.nhl.com" pointing at flask container
    3. Figure out SSL for all this
    4. Bunch of tests where data served from flask to client, values checked
* See what pep8 things can be done 
* Document everything

### team.py

* Add some functions like record (W-L-OT) 

### person.py

* Add some functions for things like shot attempts, shot attempts on net, etc ... 

## Future Goals

* Draft info
* Prospect basics 
* Award info (maybe)
* Inspect /api/v1/franchises
* Determine what was tracked through different eras, handle increase in
  tracking over the years so all years work with this (
   try:
   except:
  for all variables set, order by what's tracked first, errors out as reach
  things formerly untracked, let people know stats not present)

## Notes

* Refs have an ID when looking at games. Returns "Object not found" when visiting the page.
* camelCase is used here instead of snake\_case due to the NHL's API using camelCase. For consistency, we use their naming scheme
* Plus-Minus starts with a _ because Plus-Minus is a terrible stat and I can make these decisions
* Many args can be used multiple times, and the statsapi will handle all of them, for example multiple seasons in the schedule arg or multiple stats on a player's stats page
