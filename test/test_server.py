from flask import Flask, Response, request, abort
import os
import sys

app = Flask(__name__)

base="/api/v1"
@app.route(base + "/game/<id_val>/feed/live")
def game_feed(id_val):
    try:
        gamefeed = open("games/{}.json".format(id_val)).read()
        resp = Response(gamefeed)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"games/{}.json\"".format(id_val), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"games/{}.json\"".format(id_val), file=sys.stderr)
        abort(404)

@app.route(base + "/people/<id_val>")
def people(id_val):
    try:
        gamefeed = open("players/{}.json".format(id_val)).read()
        resp = Response(gamefeed)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"players/{}.json\"".format(id_val), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"players/{}.json\"".format(id_val), file=sys.stderr)
        abort(404)

#Check for all the required stats lines
@app.route(base + "/people/<id_val>/stats")
def people_stats(id_val):
    args = request.args
    stats = args.getlist("stats")
    stats.sort()
    reqd_stats = ["byMonth", "gameLog", "goalsByGameSituation", "homeAndAway", "regularSeasonStatRankings", "statsSingleSeason", "vsConference", "vsDivision", "vsTeam", "winLoss"]
    if stats != reqd_stats:
        abort(404)

    try:
        gamefeed = open("players/{}.stats.json".format(id_val)).read()
        resp = Response(gamefeed)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"players/{}.stats.json\"".format(id_val), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"players/{}.stats.json\"".format(id_val), file=sys.stderr)
        abort(404)

#need ?expand=team.stats&expand=team.roster
@app.route(base + "/teams")
def teams():
    args = request.args
    expand = args.getlist("expand")
    filename = "teams/teams.json"
    if "team.stats" in args and "team.roster" in args:
        filename = "teams/teams.expanded.json"
    try:
        teamstats = open(filename).read()
        resp = Response(teamstats)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"{}\"".format(filename), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"{}\"".format(filename), file=sys.stderr)
        abort(404)

#need ?expand=team.stats&expand=team.roster
@app.route(base + "/teams/<id_val>")
def team(id_val):
    args = request.args
    expand = args.getlist("expand")
    filename = "teams/{}.json".format(id_val)
    if "team.stats" in expand and "team.roster" in expand:
        filename = "teams/{}.expanded.json".format(id_val)
    try:
        teamstats = open(filename).read()
        resp = Response(teamstats)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"{}\"".format(filename), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"{}\"".format(filename), file=sys.stderr)
        abort(404)

@app.route(base + "/schedule")
def schedule():
#args: team=
#args: season= OR date= or (startDate= and endDate=)
#season overrides team
    args = request.args
    filename = "schedule/schedule.json"

    season = args.getlist("season")
    date = args.getlist("date")
    start = args.getlist("startDate")
    end = args.getlist("endDate")
    team_id = args.getlist("teamId")

    if season != []:
        filename = "schedule/schedule.{}.json".format(season[0])
    elif start != [] and end != []:
        filename = "schedule/schedule.{}.{}.json".format(start[0], end[0])
    elif date != []:
        filename = "schedule/schedule.{}.json".format(date[0])

    if team_id != []:
       filename = "schedule/{}{}".format(team_id[0], filename.split("schedule")[2])

    try:
        teamstats = open(filename).read()
        resp = Response(teamstats)
        resp.headers["Content-Type"] = "application/json;charset=UTF-8"
        return resp
    except PermissionError:
        print("No permission to open \"{}\"".format(filename), file=sys.stderr)
        abort(403)
    except FileNotFoundError:
        print("No file \"{}\"".format(filename), file=sys.stderr)
        abort(404)

app.run()
