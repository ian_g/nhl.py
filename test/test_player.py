#!/usr/bin/env python3

import unittest
from nhl import Person
from datetime import datetime

#test_vsTeam
#test_vsConference
#test_vsDivision
#test_byMonth
#test_winloss
#test_homeAndAway

class TestForward(unittest.TestCase):
    def setUp(self):
        self.player = Person("8468508")

    def test_biographics(self):
        self.assertEqual("Justin Williams", self.player.name, "Name mismatch")
        self.assertEqual("8468508", self.player.id, "ID mismatch") #id
        self.assertEqual("14", self.player.num, "Number doesn't match") #number
        self.assertEqual(datetime.strptime("1981-10-04", "%Y-%m-%d"), self.player.birthDate) #birthdate
        self.assertEqual(37, self.player.age) #age
        self.assertEqual("6' 1\"", self.player.height) #height
        self.assertEqual(184, self.player.weight) #weight
        self.assertEqual("R", self.player.shootsCatches) #shoots/catches
        self.assertEqual("Carolina Hurricanes", self.player.team) #team
        self.assertEqual("RW", self.player.position) #position
        self.assertEqual(False, self.player.rookie) #rookie y/n
        self.assertEqual("Cobourg", self.player.birthCity) #birth city
        self.assertEqual("ON", self.player.birthStateProvince) #birth state/province
        self.assertEqual("CAN", self.player.birthCountry) #birth country
        self.assertEqual("CAN", self.player.nationalCountry) #national team

    def test_singleSplitInit(self):
        self.assertEqual(23, self.player.goals)
        self.assertEqual(30, self.player.assists)
        self.assertEqual(53, self.player.points)
        self.assertEqual(237, self.player.shots)
        self.assertEqual(32, self.player.blockedShots)
        self.assertEqual(9.7, self.player.shotPct)
        self.assertEqual(44, self.player.penaltyMinutes)
        self.assertEqual(82, self.player.gamesPlayed)

        self.assertEqual("1430:18", self.player.timeOnIce)
        self.assertEqual("17:26", self.player.timeOnIcePerGame)
        self.assertEqual("14:43", self.player.evenStrengthTimeOnIcePerGame)
        self.assertEqual("00:01", self.player.shorthandedTimeOnIcePerGame)

        self.assertEqual(9, self.player.ppGoals)
        self.assertEqual(14, self.player.ppPoints)
        self.assertEqual("222:07", self.player.ppTimeOnIce)
        self.assertEqual("02:42", self.player.ppTimeOnIcePerGame)
        self.assertEqual(5, self.player.gameWinningGoals)
        self.assertEqual(0, self.player.otGoals)

        self.assertEqual(0, self.player.shorthandedGoals)
        self.assertEqual(0, self.player.shorthandedPoints)
        self.assertEqual(0, self.player.shorthandedAssists)
        self.assertEqual("01:25", self.player.shorthandedTimeOnIce)

        self.assertEqual(38.01, self.player.faceoffPct)
        self.assertEqual(55, self.player.hits)
        self.assertEqual(-4, self.player._plusMinus)
        self.assertEqual(1814, self.player.shifts)

    def test_rankingsInit(self):
        self.assertEqual("32nd", self.player.rankPpGoals) #focusedJson["rankPowerPlayGoals"]
        self.assertEqual("410th", self.player.rankBlockedShots) #focusedJson["rankBlockedShots"]
        self.assertEqual("111th", self.player.rankAssists) #focusedJson["rankAssists"]
        self.assertEqual("304th", self.player.rankShotPct) #focusedJson["rankShotPct"]
        self.assertEqual("82nd", self.player.rankGoals) #focusedJson["rankGoals"]
        self.assertEqual("414th", self.player.rankHits) #focusedJson["rankHits"]
        self.assertEqual("121st", self.player.rankPenaltyMinutes) #focusedJson["rankPenaltyMinutes"]
        self.assertEqual("156th", self.player.rankShorthandedGoals) #focusedJson["rankShortHandedGoals"]
        self.assertEqual("26th", self.player.rankShots) #focusedJson["rankPlusMinus"]
        self.assertEqual("94th", self.player.rankPoints) #focusedJson["rankShots"]
        self.assertEqual("134th", self.player.rankOtGoals) #focusedJson["rankPoints"]
        self.assertEqual("4th", self.player.rankGamesPlayed) #focusedJson["rankOvertimeGoals"]
        self.assertEqual("582nd", self.player._rankPlusMinus) #focusedJson["rankGamesPlayed"]

    def test_goalSituationInit(self):
        actual_output = {
			"goalsInFirstPeriod": 6,
			"goalsInSecondPeriod": 6,
			"goalsInThirdPeriod": 11,
			"gameWinningGoals": 5,
			"emptyNetGoals": 0,
			"shootOutGoals": 0,
			"shootOutShots": 2,
			"goalsTrailingByOne": 6,
			"goalsTrailingByThreePlus": 1,
			"goalsWhenTied": 10,
			"goalsLeadingByOne": 5,
			"goalsLeadingByThreePlus": 1,
			"penaltyGoals": 0,
			"penaltyShots": 0,
        }
        self.assertEqual(actual_output, self.player.goalsBySituation)

    def test_gameLogInit(self):
        first = {
            "id": 2018021263,
            "win": True,
            "OT": False,
            "home": False,
            "versus": "Philadelphia Flyers",
            "versusId": 4,
            "team": "Carolina Hurricanes",
            "teamId": 12,
			"timeOnIce": "18:28",
			"assists": 0,
			"goals": 0,
			"pim": 0,
			"shots": 2,
			"games": 1,
			"hits": 1,
			"powerPlayGoals": 0,
			"powerPlayPoints": 0,
			"powerPlayTimeOnIce": "02:15",
			"evenTimeOnIce": "16:13",
			"penaltyMinutes": "0",
			"faceOffPct": 50,
			"shotPct": 0,
			"gameWinningGoals": 0,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 0,
			"shortHandedTimeOnIce": "00:00",
			"blocked": 1,
			"plusMinus": 0,
			"points": 0,
			"shifts": 22,
        }
        index_25 = {
            "id": 2018020867,
            "win": True,
            "OT": False,
            "home": False,
            "versus": "Ottawa Senators",
            "versusId": 9,
            "team": "Carolina Hurricanes",
            "teamId": 12,
			"timeOnIce": "17:46",
			"assists": 1,
			"goals": 1,
			"pim": 0,
			"shots": 5,
			"games": 1,
			"hits": 2,
			"powerPlayGoals": 1,
			"powerPlayPoints": 2,
			"powerPlayTimeOnIce": "03:24",
			"evenTimeOnIce": "14:22",
			"penaltyMinutes": "0",
			"faceOffPct": 50,
			"shotPct": 20,
			"gameWinningGoals": 1,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 0,
			"shortHandedTimeOnIce": "00:00",
			"blocked": 0,
			"plusMinus": 0,
			"points": 2,
			"shifts": 22,
        }
        last = {
            "id": 2018020008,
            "win": False,
            "OT": True,
            "home": True,
            "versus": "New York Islanders",
            "versusId": 2,
            "team": "Carolina Hurricanes",
            "teamId": 12,
			"timeOnIce": "18:49",
			"assists": 0,
			"goals": 0,
			"pim": 0,
			"shots": 5,
			"games": 1,
			"hits": 0,
			"powerPlayGoals": 0,
			"powerPlayPoints": 0,
			"powerPlayTimeOnIce": "02:57",
			"evenTimeOnIce": "15:52",
			"penaltyMinutes": "0",
			"faceOffPct": 33.33,
			"shotPct": 0,
			"gameWinningGoals": 0,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 0,
			"shortHandedTimeOnIce": "00:00",
			"blocked": 0,
			"plusMinus": 1,
			"points": 0,
			"shifts": 26,
        }
        length = 82
        self.assertEqual(length, len(self.player.gameLog))
        self.assertEqual(first, self.player.gameLog[first["id"]])
        self.assertEqual(index_25, self.player.gameLog[index_25["id"]])
        self.assertEqual(last, self.player.gameLog[last["id"]])

class TestDefense(unittest.TestCase):
    def setUp(self):
        self.player = Person("8475764")

    def test_biographics(self):
        self.assertEqual("Cam Fowler", self.player.name, "Name mismatch")
        self.assertEqual("8475764", self.player.id, "ID mismatch") #id
        self.assertEqual("4", self.player.num, "Number doesn't match") #number
        self.assertEqual(datetime.strptime("1991-12-05", "%Y-%m-%d"), self.player.birthDate) #birthdate
        self.assertEqual(27, self.player.age) #age
        self.assertEqual("6' 2\"", self.player.height) #height
        self.assertEqual(206, self.player.weight) #weight
        self.assertEqual("L", self.player.shootsCatches) #shoots/catches
        self.assertEqual("Anaheim Ducks", self.player.team) #team
        self.assertEqual("D", self.player.position) #position
        self.assertEqual(False, self.player.rookie) #rookie y/n
        self.assertEqual("Windsor", self.player.birthCity) #birth city
        self.assertEqual("ON", self.player.birthStateProvince) #birth state/province
        self.assertEqual("CAN", self.player.birthCountry) #birth country
        self.assertEqual("USA", self.player.nationalCountry) #national team

    def test_singleSplitInit(self):
        self.assertEqual(5, self.player.goals)
        self.assertEqual(18, self.player.assists)
        self.assertEqual(23, self.player.points)
        self.assertEqual(88, self.player.shots)
        self.assertEqual(91, self.player.blockedShots)
        self.assertEqual(5.7, self.player.shotPct)
        self.assertEqual(20, self.player.penaltyMinutes)
        self.assertEqual(59, self.player.gamesPlayed)

        self.assertEqual("1378:42", self.player.timeOnIce)
        self.assertEqual("23:22", self.player.timeOnIcePerGame)
        self.assertEqual("19:21", self.player.evenStrengthTimeOnIcePerGame)
        self.assertEqual("01:30", self.player.shorthandedTimeOnIcePerGame)

        self.assertEqual(1, self.player.ppGoals)
        self.assertEqual(6, self.player.ppPoints)
        self.assertEqual("148:04", self.player.ppTimeOnIce)
        self.assertEqual("02:30", self.player.ppTimeOnIcePerGame)
        self.assertEqual(2, self.player.gameWinningGoals)
        self.assertEqual(1, self.player.otGoals)

        self.assertEqual(0, self.player.shorthandedGoals)
        self.assertEqual(1, self.player.shorthandedPoints)
        self.assertEqual(1, self.player.shorthandedAssists)
        self.assertEqual("88:52", self.player.shorthandedTimeOnIce)

        self.assertEqual(0, self.player.faceoffPct)
        self.assertEqual(32, self.player.hits)
        self.assertEqual(-14, self.player._plusMinus)
        self.assertEqual(1727, self.player.shifts)

    def test_goalieRankingsInit(self):
        self.assertEqual("263rd", self.player.rankPpGoals)
        self.assertEqual("107th", self.player.rankBlockedShots)
        self.assertEqual("269th", self.player.rankAssists)
        self.assertEqual("530th", self.player.rankShotPct)
        self.assertEqual("422nd", self.player.rankGoals)
        self.assertEqual("577th", self.player.rankHits)
        self.assertEqual("392nd", self.player.rankPenaltyMinutes)
        self.assertEqual("156th", self.player.rankShorthandedGoals)
        self.assertEqual("413th", self.player.rankShots)
        self.assertEqual("346th", self.player.rankPoints)
        self.assertEqual("37th", self.player.rankOtGoals)
        self.assertEqual("461st", self.player.rankGamesPlayed)
        self.assertEqual("829th", self.player._rankPlusMinus)

    def test_goalSituationInit(self):
        actual_output = {
            "goalsInFirstPeriod" : 1,
            "goalsInSecondPeriod" : 3,
            "goalsInOvertime" : 1,
            "goalsTrailingByOne" : 1,
            "goalsWhenTied" : 3,
            "goalsLeadingByOne" : 1
        }
        self.assertEqual(actual_output, self.player.goalsBySituation)

    def test_gameLogInit(self):
        first = {
            "id": 2018021256,
            "win": True,
            "OT": False,
            "home": True,
            "versus": "Los Angeles Kings",
            "versusId": 26,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "22:45",
			"assists": 2,
			"goals": 0,
			"pim": 0,
			"shots": 0,
			"games": 1,
			"hits": 0,
			"powerPlayGoals": 0,
			"powerPlayPoints": 0,
			"powerPlayTimeOnIce": "00:59",
			"evenTimeOnIce": "19:44",
			"penaltyMinutes": "0",
			"gameWinningGoals": 0,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 1,
			"shortHandedTimeOnIce": "02:02",
			"blocked": 0,
			"plusMinus": -2,
			"points": 2,
			"shifts": 30,
        }
        index_25 = {
            "id": 2018020876,
            "win": True,
            "OT": False,
            "home": True,
            "versus": "Vancouver Canucks",
            "versusId": 23,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "22:42",
			"assists": 0,
			"goals": 0,
			"pim": 0,
			"shots": 0,
			"games": 1,
			"hits": 2,
			"powerPlayGoals": 0,
			"powerPlayPoints": 0,
			"powerPlayTimeOnIce": "02:27",
			"evenTimeOnIce": "17:18",
			"penaltyMinutes": "0",
			"gameWinningGoals": 0,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 0,
			"shortHandedTimeOnIce": "02:57",
			"blocked": 2,
			"plusMinus": 1,
			"points": 0,
			"shifts": 27,
        }
        last = {
            "id": 2018020004,
            "win": True,
            "OT": False,
            "home": False,
            "versus": "San Jose Sharks",
            "versusId": 28,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "24:07",
			"assists": 0,
			"goals": 0,
			"pim": 0,
			"shots": 0,
			"games": 1,
			"hits": 0,
			"powerPlayGoals": 0,
			"powerPlayPoints": 0,
			"powerPlayTimeOnIce": "01:27",
			"evenTimeOnIce": "21:19",
			"penaltyMinutes": "0",
			"gameWinningGoals": 0,
			"overTimeGoals": 0,
			"shortHandedGoals": 0,
			"shortHandedPoints": 0,
			"shortHandedTimeOnIce": "01:21",
			"blocked": 1,
			"plusMinus": 0,
			"points": 0,
			"shifts": 33,
        }
        length = 59
        self.assertEqual(length, len(self.player.gameLog))
        self.assertEqual(first, self.player.gameLog[first["id"]])
        self.assertEqual(index_25, self.player.gameLog[index_25["id"]])
        self.assertEqual(last, self.player.gameLog[last["id"]])

class TestGoalie(unittest.TestCase):
    def setUp(self):
        self.player = Person("8476434")

    def test_biographics(self):
        self.assertEqual("John Gibson", self.player.name, "Name mismatch")
        self.assertEqual("8476434", self.player.id, "ID mismatch") #id
        self.assertEqual("36", self.player.num, "Number doesn't match") #number
        self.assertEqual(datetime.strptime("1993-07-14", "%Y-%m-%d"), self.player.birthDate) #birthdate
        self.assertEqual(25, self.player.age) #age
        self.assertEqual("6' 2\"", self.player.height) #height
        self.assertEqual(206, self.player.weight) #weight
        self.assertEqual("L", self.player.shootsCatches) #shoots/catches
        self.assertEqual("Anaheim Ducks", self.player.team) #team
        self.assertEqual("G", self.player.position) #position
        self.assertEqual(False, self.player.rookie) #rookie y/n
        self.assertEqual("Pittsburgh", self.player.birthCity) #birth city
        self.assertEqual("PA", self.player.birthStateProvince) #birth state/province
        self.assertEqual("USA", self.player.birthCountry) #birth country
        self.assertEqual("USA", self.player.nationalCountry) #national team

    def test_singleSplitGoalieInit(self):
        self.assertEqual("3233:02", self.player.timeOnIce)
        self.assertEqual(8, self.player.ot)
        self.assertEqual(2, self.player.shutouts)
        self.assertEqual(0, self.player.ties)
        self.assertEqual(26, self.player.wins)
        self.assertEqual(22, self.player.losses)
        self.assertEqual(1685, self.player.saves)
        self.assertEqual(285, self.player.ppSaves)
        self.assertEqual(43, self.player.shSaves)
        self.assertEqual(1357, self.player.evenSaves)
        self.assertEqual(46, self.player.shShots)
        self.assertEqual(1470, self.player.evenShots)
        self.assertEqual(322, self.player.ppShots)
        self.assertEqual(0.917, self.player.savePct)
        self.assertEqual(2.84, self.player.goalsAgainstAverage)
        self.assertEqual(58, self.player.games)
        self.assertEqual(57, self.player.gamesStarted)
        self.assertEqual(1838, self.player.shotsAgainst)
        self.assertEqual(153, self.player.goalsAgainst)
        self.assertEqual("55:44", self.player.timeOnIcePerGame)
        self.assertEqual(88.50931677018633, self.player.ppSavePct)
        self.assertEqual(93.47826086956522, self.player.shSavePct)
        self.assertEqual(92.31292517006803, self.player.evenSavePct)

    def test_rankingsInit(self):
        self.assertEqual("6th", self.player.rankShotsAgainst)
        self.assertEqual("4th", self.player.rankOt)
        self.assertEqual("16th", self.player.rankPenaltyMinutes)
        self.assertEqual("11th", self.player.rankTimeOnIce)
        self.assertEqual("23rd", self.player.rankShutouts)
        self.assertEqual("6th", self.player.rankSaves)
        self.assertEqual("10th", self.player.rankLosses)
        self.assertEqual("11th", self.player.rankGoalsAgainst)
        self.assertEqual("16th", self.player.rankSavePercentage)
        self.assertEqual("10th", self.player.rankGames)
        self.assertEqual("29th", self.player.rankGoalsAgainstAverage)
        self.assertEqual("17th", self.player.rankWins)

    def test_goalSituationInit(self):
        actual_output = {
			"goalsInFirstPeriod": 49,
			"goalsInSecondPeriod": 55,
			"goalsInThirdPeriod": 43,
			"goalsInOvertime": 6,
			"shootOutGoals": 4,
			"shootOutShots": 13,
			"goalsTrailingByOne": 26,
			"goalsTrailingByTwo": 12,
			"goalsTrailingByThreePlus": 7,
			"goalsWhenTied": 56,
			"goalsLeadingByOne": 30,
			"goalsLeadingByTwo": 15,
			"goalsLeadingByThreePlus": 7,
			"penaltyGoals": 0,
			"penaltyShots": 0,
        }
        self.assertEqual(actual_output, self.player.goalsBySituation)

    def test_gameLogInit(self):
        first = {
            "id": 2018021256,
            "win": True,
            "OT": False,
            "home": True,
            "versus": "Los Angeles Kings",
            "versusId": 26,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "60:00",
			"ot": 0,
			"shutouts": 0,
			"saves": 44,
			"powerPlaySaves": 11,
			"shortHandedSaves": 1,
			"evenSaves": 32,
			"shortHandedShots": 1,
			"evenShots": 34,
			"powerPlayShots": 11,
			"decision": "W",
			"savePercentage": 0.957,
			"games": 1,
			"gamesStarted": 1,
			"shotsAgainst": 46,
			"goalsAgainst": 2,
			"powerPlaySavePercentage": 100,
			"shortHandedSavePercentage": 100,
			"evenStrengthSavePercentage": 94.11764705882352,
        }
        index_25 = {
            "id": 2018020597,
            "win": False,
            "OT": True,
            "home": True,
            "versus": "Arizona Coyotes",
            "versusId": 53,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "60:16",
			"ot": 1,
			"shutouts": 0,
			"saves": 25,
			"powerPlaySaves": 2,
			"shortHandedSaves": 2,
			"evenSaves": 21,
			"shortHandedShots": 2,
			"evenShots": 25,
			"powerPlayShots": 3,
			"decision": "O",
			"savePercentage": 0.833,
			"games": 1,
			"gamesStarted": 1,
			"shotsAgainst": 30,
			"goalsAgainst": 5,
			"powerPlaySavePercentage": 66.66666666666666,
			"shortHandedSavePercentage": 100,
			"evenStrengthSavePercentage": 84,
        }
        last = {
            "id": 2018020004,
            "win": True,
            "OT": False,
            "home": False,
            "versus": "San Jose Sharks",
            "versusId": 28,
            "team": "Anaheim Ducks",
            "teamId": 24,
			"timeOnIce": "60:00",
			"ot": 0,
			"shutouts": 0,
			"saves": 31,
			"powerPlaySaves": 1,
			"shortHandedSaves": 0,
			"evenSaves": 30,
			"shortHandedShots": 1,
			"evenShots": 31,
			"powerPlayShots": 1,
			"decision": "W",
			"savePercentage": 0.939,
			"games": 1,
			"gamesStarted": 1,
			"shotsAgainst": 33,
			"goalsAgainst": 2,
			"powerPlaySavePercentage": 100,
			"shortHandedSavePercentage": 0,
			"evenStrengthSavePercentage": 96.7741935483871,
        }
        length = 58
        self.assertEqual(length, len(self.player.gameLog))
        self.assertEqual(first, self.player.gameLog[first["id"]])
        self.assertEqual(index_25, self.player.gameLog[index_25["id"]])
        self.assertEqual(last, self.player.gameLog[last["id"]])

if __name__ == "__main__":
    unittest.main()

#    #homeAndAway, winLoss, byMonth, vsDivision, vsConference, vsTeam
#    def splitsInit(self, ix):
#        """Set comparative stats for a player in a season"""
#        name = self.statsJson[ix]["type"]["displayName"]
#        focusedJson = self.statsJson[ix]["splits"]
#        splits = {}
#        if name == "homeAndAway":
#            if focusedJson[0]["isHome"]:
#                splits["home"] = focusedJson[0]
#                splits["away"] = focusedJson[1]
#            else:
#                splits["home"] = focusedJson[1]
#                splits["away"] = focusedJson[0]
#        elif name == "vsDivision":
#            for div in focusedJson:
#                divisionLog = {
#                    "division": div["opponentDivision"]["name"],
#                    "divisionId": div["opponentDivision"]["id"],
#                    "conference": div["opponentConference"]["name"],
#                    "conferenceId": div["opponentConference"]["id"],
#                }
#                divisionLog.update(div["stat"])
#                splits[div["opponentDivision"]["name"]] = divisionLog
#        elif name == "vsConference":
#            for conf in focusedJson:
#                conferenceLog = conf["stat"]
#                conferenceLog["conference"] = conf["opponentConference"]["name"]
#                conferenceLog["conferenceId"] = conf["opponentConference"]["id"]
#                splits[conf["opponentConference"]["name"]] = conferenceLog
#        elif name == "vsTeam":
#            for team in focusedJson:
#                teamLog = team["stat"]
#                teamLog["team"] = team["opponent"]["name"]
#                teamLog["teamId"] = team["opponent"]["id"]
#                teamLog["division"] = team["opponentDivision"]["name"]
#                teamLog["divisionId"] = team["opponentDivision"]["id"]
#                teamLog["conference"] = team["opponentConference"]["name"]
#                teamLog["conferenceId"] = team["opponentConference"]["id"]
#                splits[team["opponentConference"]["name"]] = teamLog
#        elif name == "winLoss":
#            for situation in focusedJson:
#                statLog = situation["stat"]
#                if situation["isWin"]:
#                    splits["win"] = statLog
#                elif situation["isOT"]:
#                    splits["loss"] = statLog
#                else: #Both false
#                    splits["otLoss"] = statLog
#        elif name == "byMonth":
#            for month in focusedJson:
#                splits[month["month"]] = month["stat"]
#        else:
#            print("Unknown stats identifier {}. Please report to code author!".format(name))
#        self.splits[name] = splits
