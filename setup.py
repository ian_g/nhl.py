#!/usr/bin/env python
from distutils.core import setup

setup(name="NHL.py",
      version="0.0.0",
      description="NHL Stats API Wrapper",
      author="Ian Gallmeister",
      author_email="iang4llmeister@gmail.com",
      url="https://gitlab.com/ian_g/nhl.py",
      packages=["nhl"]
     )
