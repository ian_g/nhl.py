from datetime import datetime
from datetime import timedelta
from .session import Connection
from .game import Game

#TODO - Test, write tests
#TODO - Test bad date formats
#TODO - Test mix of kwargs
#TODO - Test display

class Schedule(Connection):
    """An NHL Game schedule for some set of dates and/or teams"""
    # pylint: disable=invalid-name
    def __init__(self, **kwargs):
        Connection.__init__(self)
        self.games = {}
        self.baseAddress = "/schedule"
        self.team = None
        self.season = None
        #Overrides start and end date
        self.date = None
        self.startDate = None
        #Defaults to the day after startDate
        self.endDate = None
        self.gameIds = {}
        self.gameBasics = {}

        #kwargs = { "season": "YYYY", \
        #           "teamId": ##, \
        #           "teamName":"name", \
        #           "teamAbbr": "ABR", \
        #           "date": "YYYY/MM/DD", \
        #           "startDate": "YYYY/MM/DD", \
        #           "endDate": "YYYY/MM/DD" \
        #           "preseason": True, \
        #           "playoffs": True }\
        URL = "{}/teams".format(self.baseURL)
        try:
            check = self.session.get(URL).json()["teams"]
        except ValueError:
            raise ValueError("No output present to process at {}".format(URL))
        except KeyError:
            raise KeyError("Data issue - no key \"teams\" - at URL: {}".format(URL))

        if "teamId" in kwargs:
            for team in check:
                if team["id"] == kwargs["teamId"]:
                    self.team = kwargs["teamId"]
            print("Team ID: {} not found".format(kwargs["teamId"]))
        elif "teamAbbr" in kwargs:
            for team in check:
                if team["abbreviation"] == kwargs["teamAbbr"]:
                    self.team = team["id"]
            print("Team Abbreviation: {} not found".format(kwargs["teamAbbr"]))
        elif "teamName" in kwargs:
            for team in check:
                if team["shortName"] == kwargs["teamName"]:
                    self.team = team["id"]
                elif team["name"] == kwargs["teamName"]:
                    self.team = team["id"]
                elif team["teamName"] == kwargs["teamName"]:
                    self.team = team["id"]
            print("Team name: {} not found".format(kwargs["teamName"]))

        if "season" in kwargs:
            season = kwargs["season"][:4]
            if not season.isdigit():
                raise ValueError("Season: {} is not an valid year".format(kwargs["season"]))
            if int(season) < 1908 or int(season) > int(datetime.now().strftime("%Y")):
                raise ValueError("Season: {} is out of the valid range".format(season))
            self.season = "{}{}".format(season, int(season)+1)
        elif "date" in kwargs:
            checkDate(kwargs["date"])
            self.date = kwargs["date"]
        elif "startDate" in kwargs:
            checkDate(kwargs["startDate"])
            self.startDate = kwargs["startDate"]
            if "endDate" in kwargs:
                checkDate(kwargs["endDate"])
                self.endDate = kwargs["endDate"]
                timePeriod = datetime.strptime(self.startDate, "%Y-%m-%d") - \
                             datetime.strptime(self.endDate, "%Y-%m-%d")
                if timePeriod > timedelta(years=1):
                    newEndDate = datetime.strptime(self.startDate, "%Y-%m-%d")
                    newEndDate += timedelta(years=1)
                    self.endDate = newEndDate.strftime("%Y-%m-%d")
                    err = "Time period greater than a full season. Limiting to one year from {}"\
                          .format(self.startDate)
                    print(err)
            else:
                self.date = self.startDate
                self.startDate = None
        self.loadGames()

    @staticmethod
    def checkDate(date):
        """Make sure date provided is correctly formatted"""
        try:
            datetime.strptime(date, '%Y-%m-%d')
        except ValueError:
            raise ValueError("Incorrect data format - needs to be YYYY-MM-DD\nExiting . . .")

    def loadGames(self):
        """Load games given initial constraints"""
        address = self.baseAddress
        modifiers = []
        if self.team:
            modifiers.append("teamId={}".format(self.team))

        if self.season:
            modifiers.append("season={}".format(self.season))
        elif self.date:
            modifiers.append("date={}".format(self.date))
        elif self.startDate:
            modifiers.append("startDate={}".format(self.startDate))
            modifiers.append("endDate={}".format(self.endDate))
        address += "?"
        address += "&".join(modifiers)

        self.URL = self.baseURL + address

        #wrap in try block
        games = {} 
        try:
            games = self.session.get(self.URL).json()
        except ValueError:
            raise ValueError("No output present to process at {}".format(self.URL))
        self.gameIds = []

        totalGames = games["totalItems"]

        for date in games["dates"]:
            for game in date["games"]:
                #print(game["gamePk"])
                self.gameIds.append(game["gamePk"])
                if totalGames < 82:
                    print(totalGames)
                    self.games[game["gamePk"]] = Game(gameId=game["gamePk"])
                self.gameBasics[game["gamePk"]] = {
                    "home": game["teams"]["home"]["team"]["name"],
                    "away": game["teams"]["away"]["team"]["name"],
                    "time": datetime.strptime(game["gameDate"], "%Y-%m-%dT%H:%M:%SZ")
                }

    def display(self):
        """Display Basic Schedule Information"""
        for game in self.gameBasics:
            info = "{}: {} @ {} - {}".format(
                game,
                self.gameBasics[game]["away"],
                self.gameBasics[game]["home"],
                self.gameBasics[game]["time"].strftime("%Y-%m-%d")
            )
            print(info)

    def __repr__(self):
        """Define representation of object in python console"""
        today = datetime.now().strftime("%Y-%m-%d")
        time = today
        if self.season:
            time = self.season
        elif self.date:
            time = self.date
        elif self.startDate:
            time = self.date
            if self.endDate:
                time += "- " + today
        return "NHL Stats API Schedule Interface: {}".format(time)

    def __getitem__(self, gameId):
        """Use Game ID to load a particular game"""
        if gameId not in self.gameIds:
            raise KeyError("No game with id {} in schedule set")

        game = Game(gameId=gameId)
        self.games[gameId] = game
        return game
