"""A holder for an NHL game and its events"""
from __future__ import print_function

import sys
import time
from datetime import datetime, timedelta
from .session import Connection
from .team import Team
from .person import Person

#TODO - Test + write tests

class Game(Connection):
    # pylint: disable=invalid-name
    def __init__(self, gameId):
        Connection.__init__(self)
        self.regular = True
        self.preseason = False
        self.playoffs = False
        self.gameId = gameId
        self.URL = self.baseURL + "/game/{}/feed/live".format(gameId)

        try:
            self.json = self.session.get(self.URL).json()
        except ValueError:
            raise ValueError("No output present to process at {}".format(self.URL))

        self.loadFromJson()

    def loadFromJson(self):
        """From JSON downloaded in __init__, set variables"""
        #Game Type
        if self.json["gameData"]["game"]["type"] == "PR":
            self.preseason = True
            self.regular = False
        elif self.json["gameData"]["game"]["type"] == "P":
            self.playoffs = True
            self.regular = False

        #Game Logistics (Starttime, State, Location)
        self.state = self.json["gameData"]["status"]["detailedState"]
        #detailedState: Scheduled (1), Final (7), In Progress (3)

        UTCStartTime = datetime.strptime(self.json["gameData"]["datetime"]["dateTime"], \
                                           "%Y-%m-%dT%H:%M:%SZ")
        localOffset = time.timezone if time.localtime().tm_isdst == 0 \
                                     else time.altzone
        self.startTime = UTCStartTime - timedelta(seconds=localOffset)
        self.venue = self.json["gameData"]["venue"]["name"]
        try:
            self.venueId = self.json["gameData"]["venue"]["id"]
        except KeyError:
            print("INFO: No venue ID - please use names for venue-based analysis")
            self.venueId = None

        #Teams
        season = self.json["gameData"]["game"]["season"]
        self.home = Team(teamId=self.json["gameData"]["teams"]["home"]["id"], season=season)
        self.away = Team(teamId=self.json["gameData"]["teams"]["away"]["id"], season=season)

        if self.state != "Scheduled":
            #Events (separate class)
            self.events = Events(self.json["liveData"]["plays"])

            #Score
            self.score = {self.home.abbr: 0, self.away.abbr: 0}
            for goal in self.events.goals:
                self.score[self.events[goal].team] += 1

            #Penalty Minutes
            self.pim = {self.home.abbr: 0, self.away.abbr: 0}
            for penalty in self.events.penalties:
                self.score[self.events[penalty].team] += 1

            self.hasOT = False
            if len(self.json["liveData"]["linescore"]["periods"]) == 4:
                self.hasOT = True
            self.hasShootout = self.json["liveData"]["linescore"]["hasShootout"]

        #Players
        self.players = {}
        self.playerIds = []
        for playerId in self.json["gameData"]["players"]:
            self.playerIds.append(playerId)

        #Officials
        self.referees = {}
        self.refereeIds = []
        self.linesmen = {}
        self.linesmanIds = []
        for official in self.json["liveData"]["boxscore"]["officials"]:
            if official["officialType"] == "Referee":
                self.refereeIds.append(official["id"])
            elif official["officialType"] == "Linesman":
                self.linesmanIds.append(official["id"])
            else:
                errMsg = "Unknown offical type: {}".format(official["officialType"])
                errMsg += "  Please report this to the developers"
                raise ValueError(errMsg)

    #Unable to access without additional data
    #def cacheReferees(self, refereeId):
    #    ref = Player(refereeId)
    #    self.referees[refereeId] = ref
    #    return ref

    #Unable to access without additional data
    #def cacheLinesman(self, refereeId):
    #    lman = Player(refereeId)
    #    self.linesmen[refereeId] = lman
    #    return lman

    def cachePlayer(self, playerId):
        """Initialize and return a Person object. Save for later too"""
        player = Person(playerId)
        self.players[playerId] = player
        return player

    #Unable to access without additional data
    #def getLinesmen(self):
    #    """Get linesmen objects"""
    #    if self.linesmen != {}:
    #        return self.linesmen
    #    else:
    #        return self.cacheLinesmen()

    #Unable to access without additional data
    #def getReferees(self):
    #    """Get referee objects"""
    #    if self.referees != {}:
    #        return self.referees
    #    return self.cacheReferees()

    def getPlayer(self, playerId):
        """Get player object"""
        try:
            return self.players[playerId]
        except KeyError:
            return self.cachePlayer(playerId)

    def getTeam(self, team):
        """Return Team object for home or away team"""
        if team not in (self.home.id, self.home.abbr, self.away.id, self.away.abbr):
            raise ValueError("Input must be ID or abbreviation of team playing in this game")
        if team == self.home.id or team == self.home.abbr:
            return self.home
        return self.away

    def display(self):
        """Create display string"""
        disp = "{away} @ {home} - {startTime}{tz} - {state} - {venue}".format(
            away=self.away.name,
            home=self.home.name,
            startTime=self.startTime.strftime("%Y/%m/%d %H:%M"),
            tz=time.timezone[time.localtime().tm_isdst],
            venue=self.venue,
        )
        return disp

    def __repr__(self):
        self.display()

    class Events:
        def __repr__(self):
            return "Events of {} @ {}".format(self.home, self.away)

        def __init__(self, events):
            self.shootoutAway = None
            self.shootoutHome = None

            self.home = events["linescore"]["teams"]["home"]["triCode"]
            self.away = events["linescore"]["teams"]["away"]["triCode"]
            self.scoring = events["plays"]["scoringPlays"]
            self.penalties = events["plays"]["penaltyPlays"]

            self.firstEnd = events["plays"]["playsByPeriod"][0]["endIndex"] + 1
            self.secondEnd = events["plays"]["playsByPeriod"][1]["endIndex"] + 1
            self.thirdEnd = events["plays"]["playsByPeriod"][2]["endIndex"]
            self.OTEnd = None
            self.hasOT = False
            if len(events["linescore"]["periods"]) == 4:
                self.hasOT = True
                self.OTEnd = events["plays"]["playsByPeriod"][3]["endIndex"]
                self.thirdEnd += 1

            self.hasShootout = events["linescore"]["hasShootout"]
            if self.hasShootout:
                self.shootoutAway = [
                    events["linescore"]["shootoutInfo"]["away"]["attempts"],
                    events["linescore"]["shootoutInfo"]["away"]["scores"],
                ]

                self.shootoutHome = [
                    events["linescore"]["shootoutInfo"]["home"]["attempts"],
                    events["linescore"]["shootoutInfo"]["home"]["scores"],
                ]

            self.winner = events["decisions"]["winner"]["id"]
            self.loser = events["decisions"]["loser"]["id"]
            self.stars = [
                events["decisions"]["firstStar"]["id"],
                events["decisions"]["secondStar"]["id"],
                events["decisions"]["thirdStar"]["id"],
            ]

            self.events = [Event(event) for event in events["plays"]["allPlays"]]

            #For caching later if requested
            self.goalsHome = None
            self.goalsAway = None
            self.pimHome = None
            self.pimAway = None
            self.shotsHome = None
            self.shotsAway = None
            self.missedSogHome = None
            self.missedSogAway = None
            self.blockedSogHome = None
            self.blockedSogAway = None
            self.giveawaysHome = None
            self.giveawaysAway = None
            self.takeawaysHome = None
            self.takeawaysAway = None
            self.hitsHome = None
            self.hitsAway = None
            self.faceoffsHome = None
            self.faceoffsAway = None

        def goals(self):
            """Get goals by period home and away"""
            if self.goalsHome:
                return [self.goalsHome, self.goalsAway]
            self.collectStats()
            return [self.goalsHome, self.goalsAway]

        def pim(self):
            """Get penalties in minutes by period home and away"""
            if self.pimHome:
                return [self.pimHome, self.pimAway]
            self.collectStats()
            return [self.pimHome, self.pimAway]

        def sog(self):
            """Get shots on goal by period home and away"""
            if self.shotsHome:
                return [self.shotsHome, self.shotsAway]
            self.collectStats()
            return [self.shotsHome, self.shotsAway]

        def missedSog(self):
            """Get missed shots on goal by period home and away"""
            if self.missedSogHome:
                return [self.missedSogHome, self.missedSogAway]
            self.collectStats()
            return [self.missedSogHome, self.missedSogAway]

        def blockedSog(self):
            """Get blocked shots on goal by period home and away"""
            if self.blockedSogHome:
                return [self.blockedSogHome, self.blockedSogAway]
            self.collectStats()
            return [self.blockedSogHome, self.blockedSogAway]

        def hits(self):
            """Get hits by period home and away"""
            if self.hitsHome:
                return [self.hitsHome, self.hitsAway]
            self.collectStats()
            return [self.hitsHome, self.hitsAway]

        def giveaways(self):
            """Get giveaways by period home and away"""
            if self.giveawaysHome:
                return [self.giveawaysHome, self.giveawaysAway]
            self.collectStats()
            return [self.giveawaysHome, self.giveawaysAway]

        def takeaways(self):
            """Get takeaways by period home and away"""
            if self.takeawaysHome:
                return [self.takeawaysHome, self.takeawaysAway]
            self.collectStats()
            return [self.takeawaysHome, self.takeawaysAway]

        def faceoffs(self):
            """Get faceoffs by period home and away"""
            if self.faceoffsHome:
                return [self.faceoffsHome, self.faceoffsAway]
            self.collectStats()
            return [self.faceoffsHome, self.faceoffsAway]

        def shootout(self):
            """Get shootout stats home and away"""
            if self.shootoutHome:
                return [self.shootoutHome, self.shootoutAway]
            print("No shootout this game")
            return None 

        def collectStats(self):
            """Parse all events and calculate stats. Cache for later retrieval"""
            self.goalsHome = [0, 0, 0]
            self.goalsAway = [0, 0, 0]
            self.pimHome = [0, 0, 0]
            self.pimAway = [0, 0, 0]
            self.shotsHome = [0, 0, 0]
            self.shotsAway = [0, 0, 0]
            self.missedSogHome = [0, 0, 0]
            self.missedSogAway = [0, 0, 0]
            self.blockedSogHome = [0, 0, 0]
            self.blockedSogAway = [0, 0, 0]
            self.giveawaysHome = [0, 0, 0]
            self.giveawaysAway = [0, 0, 0]
            self.takeawaysHome = [0, 0, 0]
            self.takeawaysAway = [0, 0, 0]
            self.hitsHome = [0, 0, 0]
            self.hitsAway = [0, 0, 0]
            self.faceoffsHome = [0, 0, 0]
            self.faceoffsAway = [0, 0, 0]
            if self.hasOT:
                self.goalsHome.append(0)
                self.goalsAway.append(0)
                self.pimHome.append(0)
                self.pimAway.append(0)
                self.shotsHome.append(0)
                self.shotsAway.append(0)
                self.missedSogHome.append(0)
                self.missedSogAway.append(0)
                self.blockedSogHome.append(0)
                self.blockedSogAway.append(0)
                self.giveawaysHome.append(0)
                self.giveawaysAway.append(0)
                self.takeawaysHome.append(0)
                self.takeawaysAway.append(0)
                self.hitsHome.append(0)
                self.hitsAway.append(0)
                self.faceoffsHome.append(0)
                self.faceoffsAway.append(0)
            for event in self.events:
                if event.period == 5:
                    continue
                if event.type == "SHOT":
                    if event.team == self.home:
                        self.shotsHome[event.period-1] += 1
                    else:
                        self.shotsAway[event.period-1] += 1
                elif event.type == "GOAL":
                    if self.home == event.team:
                        self.goalsHome[event.period-1] += 1
                        self.shotsHome[event.period-1] += 1
                    else:
                        self.goalsAway[event.period-1] += 1
                        self.shotsAway[event.period-1] += 1
                elif event.type == "MISSED_SHOT":
                    if self.home == event.team:
                        self.missedSogHome[event.period-1] += 1
                    else:
                        self.missedSogAway[event.period-1] += 1
                elif event.type == "BLOCKED_SHOT":
                    if self.home == event.team:
                        self.blockedSogHome[event.period-1] += 1
                    else:
                        self.blockedSogAway[event.period-1] += 1
                elif event.type == "HIT":
                    if self.home == event.team:
                        self.hitsHome[event.period-1] += 1
                    else:
                        self.hitsAway[event.period-1] += 1
                elif event.type == "PENALTY":
                    if self.home == event.team:
                        self.pimHome[event.period-1] += event.penaltyMinutes
                    else:
                        self.pimAway[event.period-1] += event.penaltyMinutes
                elif event.type == "GIVEAWAY":
                    if self.home == event.team:
                        self.giveawaysHome[event.period-1] += 1 
                    else:
                        self.giveawaysAway[event.period-1] += 1
                elif event.type == "TAKEAWAY":
                    if self.home == event.team:
                        self.takeawaysHome[event.period-1] += 1 
                    else:
                        self.takeawaysAway[event.period-1] += 1
                elif event.type == "FACEOFF":
                    if self.home == event.team:
                        self.faceoffsHome[event.period-1] += 1 
                    else:
                        self.faceoffsAway[event.period-1] += 1

    class Event:
        """An event in a hockey game"""
        def __repr__(self):
            return "  {} - {} - {}".format(self.type, self.period, self.periodTime)

        def __init__(self, event):
            self.coordinates = None
            self.players = {}
            self.team = None
            self.penalty = None

            #Logistical
            self.period = event["about"]["period"]
            self.periodTime = event["about"]["periodTime"]
            UTCEventTime = datetime.strptime(event["about"]["dateTime"],
                                             "%Y-%m-%dT%H:%M:%SZ")
            localOffset = time.timezone if time.localtime().tm_isdst == 0\
                                         else time.altzone
            self.eventTime = UTCEventTime - timedelta(seconds=localOffset)
            #"GAME_SCHEDULED", "PERIOD_READY", "PERIOD_START", "FACEOFF", \
            #"BLOCKED_SHOT", "STOP", "GIVEAWAY", "SHOT", "HIT", "MISSED_SHOT",\
            #"TAKEAWAY", "GOAL", "PERIOD_END", "PERIOD_OFFICIAL", "PENALTY",\
            #"GAME_END", "GAME_OFFICIAL"
            self.type = event["result"]["eventTypeId"]
            #On-Ice Event
            if self.type == "PENALTY":
                self.penalty = {
                    "type": event["result"]["secondaryType"],
                    "length": event["result"]["penaltyMinutes"],
                    "severity": event["result"]["penaltySeverity"]
                }
            if "players" in event:
                for player in event["players"]:
                    #{ID: player_type}
                    #"Winner", "Loser", "Blocker", "Shooter", "PlayerID", \
                    #"Goalie", "Hitter", "Hittee", "Scorer", "Assist", \
                    #"PenaltyOn", "DrewBy"
                    self.players[player["player"]["id"]] = player["playerType"]
            if event["coordinates"] != {}:
                #{x: 0, y: 0}
                self.coordinates = event["coordinates"]
            if "team" in event:
                self.team = event["team"]["triCode"]
