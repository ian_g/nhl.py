"""A connection class for all the pieces shared among the other classes"""
import json
import requests

class Connection:
    """A connection class to serve as a dummy base for the others"""
    # pylint: disable=invalid-name
    def __init__(self):
        self.session = requests.session()
        self.baseURL = "https://statsapi.web.nhl.com/api/v1"

    def verbose_get(self, URL):
        """Make a GET request and print a curl-like response"""
        response = self.session.get(URL)
        try:
            returned = json.dumps(response.json(), sort_keys=True, indent=4)
        except ValueError:
            returned = response.content
        print("HTTP/1.1 {code} {reason} \n {headers} \n{content}\n\n".format(
            code=response.status_code,
            reason=response.reason,
            headers='\n'.join(key + ':' + value for key, value in response.headers.items()),
            content=returned
        ))
        return returned
