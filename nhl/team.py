from datetime import datetime
from .person import Person
from .session import Connection

class Team(Connection):
    """Team object with stats"""
    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=invalid-name
    def __repr__(self):
        return "{}: {} ({})".format(self.id, self.teamName, self.abbr)

    def __getitem__(self, playerId):
        if playerId in self.players:
            return self.players[playerId]
        if playerId in self.ids:
            print("id")
            player = Person(playerId)
        elif playerId in self.playerIds:
            print("name")
            player = Person(self.playerIds[playerId])
        else:
            raise KeyError("Player {} not recorded as a member of this team".format(playerId))
        self.players[playerId] = player
        return player

    def __init__(self, teamId, season=None):
        Connection.__init__(self)
        self.URL = self.baseURL + "/teams/{}?expand=team.stats&expand=team.roster".format(teamId)
        if season:
            if not season.isdigit():
                raise ValueError("Season must be a digit")
            season = season[:4]
            if int(season) > 1906 and int(season) < int(datetime.now().strftime("%Y")):
                year = int(season)
                season = "{}{}".format(year, year+1)
            elif int(season) == datetime.now().strftime("%Y"):
                if int(datetime.now().strftime("%m")) < 7:
                    year = int(season)
                season = "{}{}".format(year-1, year)
            else:
                raise ValueError("Value for season out of range")
            self.URL += "&season={}".format(season)

        #Handle some errors here pls
        try:
            self.json = self.session.get(self.URL).json()["teams"][0]
        except ValueError:
            raise ValueError("No output present to process at {}".format(self.URL))
        except KeyError:
            raise KeyError("Data issue - no key \"teams\" - at URL: {}".format(self.URL))
        except IndexError:
            raise IndexError("Data issue - no data present - at URL: {}".format(self.URL))

        #Logistical Info
        self.id = teamId
        self.fullName = self.json["name"]
        self.teamName = self.json["teamName"]
        self.abbr = self.json["abbreviation"]

        self.firstSeason = self.json["firstYearOfPlay"]

        self.city = self.json["venue"]["city"]
        self.arenaName = self.json["venue"]["name"]

        self.division = self.json["division"]["name"]
        self.conference = self.json["conference"]["name"]

        #Player basics
        self.playerIds = {player["person"]["fullName"]:player["person"]["id"]
                          for player in self.json["roster"]["roster"]}
        self.ids = [player["person"]["id"] for player in self.json["roster"]["roster"]]
        self.players = {}

        #Team numbers + rankings
        self.initTeamStats(self.json["teamStats"][0]["splits"][0]["stat"])
        self.initTeamRanks(self.json["teamStats"][0]["splits"][1]["stat"])

    def initTeamStats(self, numbers):
        """Set basic numbers for team season"""
        self.games = numbers["gamesPlayed"]
        self.wins = numbers["wins"]
        self.losses = numbers["losses"]
        self.otLosses = numbers["ot"]
        self.points = numbers["pts"]
        self.pointPercent = float(numbers["ptPctg"])

        self.ppPercent = float(numbers["powerPlayPercentage"])
        self.ppGoalsFor = numbers["powerPlayGoals"]
        self.pp = numbers["powerPlayOpportunities"]
        self.ppGoalsAgainst = numbers["powerPlayGoalsAgainst"]
        self.pkPercentage = numbers["penaltyKillPercentage"]

        self.faceoffs = numbers["faceOffsTaken"]
        self.faceoffWins = int(numbers["faceOffsWon"])
        self.faceoffLosses = int(numbers["faceOffsLost"])
        self.faceoffWinPercentage = float(numbers["faceOffWinPercentage"])

        self.shotsForPerGame = numbers["shotsPerGame"]
        self.shotsAgainstPerGame = numbers["shotsAllowed"]
        self.shootingPercentage = numbers["shootingPctg"]
        self.savePercentage = numbers["savePctg"]
        self.shotsFor = int(round(self.shotsForPerGame * self.games, 0))
        self.shotsAgainst = int(round(self.shotsAgainstPerGame * self.games, 0))
        self.goalsForPerGame = numbers["goalsPerGame"]
        self.goalsAgainstPerGame = numbers["goalsAgainstPerGame"]
        self.goalsFor = int(round(self.goalsForPerGame * self.games, 0))
        self.goalsAgainst = int(round(self.goalsAgainstPerGame * self.games, 0))

        self.winPercentageOutshooting = numbers["winOutshootOpp"]
        self.winPercentageOutshot = numbers["winOutshotByOpp"]
        self.winPercentageScoreFirst = numbers["winScoreFirst"]
        self.winPercentageNotScoreFirst = numbers["winOppScoreFirst"]
        self.winPercentageLeadAfterFirst = numbers["winLeadFirstPer"]
        self.winPercentageLeadAfterSecond = numbers["winLeadSecondPer"]

    def initTeamRanks(self, ranks):
        """Set basic ranks for team season"""
        self.rankWins = ranks["wins"]
        self.rankLosses = ranks["losses"]
        self.rankOtLosses = ranks["ot"]
        self.rankPoints = ranks["pts"]
        self.rankPointPercent = ranks["ptPctg"]

        self.rankPowerPlayPercent = ranks["powerPlayPercentage"]
        self.rankPowerPlayGoalsFor = ranks["powerPlayGoals"]
        self.rankPowerPlays = ranks["powerPlayOpportunities"]
        self.rankPowerPlayGoalsAgainst = ranks["powerPlayGoalsAgainst"]
        self.rankPenaltyKillPercentage = ranks["penaltyKillPercentage"]

        self.rankFacoffs = ranks["faceOffsTaken"]
        self.rankFaceoffWins = ranks["faceOffsWon"]
        self.rankFaceoffLosses = ranks["faceOffsLost"]
        self.rankFaceoffWinPercentage = ranks["faceOffWinPercentage"]

        self.rankShotsForPerGame = ranks["shotsPerGame"]
        self.rankShotsAgainstPerGame = ranks["shotsAllowed"]
        self.rankShootingPercentage = ranks["shootingPctRank"]
        self.rankSavePercentage = ranks["savePctRank"]
        self.rankGoalsForPerGame = ranks["goalsPerGame"]
        self.rankGoalsAgainstPerGame = ranks["goalsAgainstPerGame"]

        self.rankWinPercentageOutshooting = ranks["winOutshootOpp"]
        self.rankWinPercentageOutshot = ranks["winOutshotByOpp"]
        self.rankWinPercentageScoreFirst = ranks["winScoreFirst"]
        self.rankWinPercentageNotScoreFirst = ranks["winOppScoreFirst"]
        self.rankWinPercentageLeadAfterFirst = ranks["winLeadFirstPer"]
        self.rankWinPercentageLeadAfterSecond = ranks["winLeadSecondPer"]
