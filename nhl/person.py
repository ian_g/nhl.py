import sys
from datetime import datetime
from datetime import timedelta
from .session import Connection

class Person(Connection):
    """An NHL Player"""
    # pylint: disable=invalid-name
    # pylint: disable=attribute-defined-outside-init
    def __repr__(self):
        return "{}: {} #{}".format(self.id, self.name, self.num)

    def __init__(self, personId, stats="statsSingleSeason", season=None, onPace=False):
        Connection.__init__(self)
        self.URL = self.baseURL + "/people/{}".format(personId)
        self.statsURL = self.URL + "/stats"

        try:
            self.json = self.session.get(self.URL).json()["people"][0]
        except ValueError:
            raise ValueError("No output present to process at {}".format(self.URL))
        except KeyError:
            raise KeyError("Data issue - no key \"people\" - at URL: {}".format(self.URL))
        except IndexError:
            raise IndexError("Data issue - no data present - at URL: {}".format(self.URL))

        self.id = personId
        self.name = self.json["fullName"]
        self.num = self.json["primaryNumber"]
        self.birthDate = datetime.strptime(self.json["birthDate"], "%Y-%m-%d")
        self.age = self.json["currentAge"]
        self.height = self.json["height"]
        self.weight = self.json["weight"]
        self.shootsCatches = self.json["shootsCatches"]
        self.team = self.json["currentTeam"]["name"]
        self.teamId = self.json["currentTeam"]["id"]
        self.position = self.json["primaryPosition"]["abbreviation"]
        self.rookie = self.json["rookie"]

        self.birthCity = self.json["birthCity"]
        self.birthStateProvince = self.json["birthStateProvince"]
        self.birthCountry = self.json["birthCountry"]
        self.nationalCountry = self.json["nationality"]

        self.gameLog = {}

        #Check season
        if season:
            try:
                now = datetime.now()
                if int(now.strftime("%m")) < 7:
                    now -= timedelta(years=1)
                now = now.strftime("%Y")
                year = datetime.strptime(season, "%Y")
                if int(year) > int(now):
                    raise ValueError
                season = "{}{}".format(year, int(year + 1))
            except ValueError:
                errorMsg = "Season value invalid\nValid years between 1906 "
                errorMsg += "and {}.\nIgnoring provided value ({})"
                errorMsg = errorMsg.format(now, season)
                print(errorMsg, file=sys.stderr)
                season = None
        self.season = season

        baseParameters = (
            ("stats", "homeAndAway"),
            ("stats", "byMonth"),
            #("stats", "byDayOfWeek"),
            ("stats", "vsDivision"),
            ("stats", "vsConference"),
            ("stats", "vsTeam"),
            ("stats", "gameLog"),
            ("stats", "winLoss"),
            ("stats", "regularSeasonStatRankings"),
            ("stats", "goalsByGameSituation"),
        )

        if onPace:
            parameters = baseParameters + (("stats", "onPaceRegularSeason"),)
        else:
            parameters = baseParameters + (("stats", "statsSingleSeason"),)

        try:
            self.statsJson = self.session.get(self.statsURL, params=parameters).json()["stats"]
        except ValueError:
            raise ValueError("No output present to process at {}".format(self.URL))
        except KeyError:
            raise KeyError("Data issue - no key \"people\" - at URL: {}".format(self.URL))

        splits = {self.statsJson[ix]["type"]["displayName"]:ix for ix in range(len(self.statsJson))}

        if onPace and self.position != "G":
            self.singleSplitInit(splits["onPaceRegularSeason"])
            del splits["onPaceRegularSeason"]
        elif not onPace and self.position != "G":
            self.singleSplitInit(splits["statsSingleSeason"])
            del splits["statsSingleSeason"]

        elif onPace:
            self.singleSplitGoalieInit(splits["onPaceRegularSeason"])
            del splits["onPaceRegularSeason"]
        else: #Is also goalie
            self.singleSplitGoalieInit(splits["statsSingleSeason"])
            del splits["statsSingleSeason"]

        self.goalSituationInit(splits["goalsByGameSituation"])
        del splits["goalsByGameSituation"]

        if self.position == "G":
            self.goalieRankingsInit(splits["regularSeasonStatRankings"])
        else:
            self.rankingsInit(splits["regularSeasonStatRankings"])
        del splits["regularSeasonStatRankings"]

        self.gameLogInit(splits["gameLog"])
        del splits["gameLog"]

        self.splits = {}
        for stat in splits:
            self.splitsInit(splits[stat])
        del splits


    #statsSingleSeason, onPaceRegularSeason
    def singleSplitInit(self, ix):
        """Initialize gross/projected stats for a player"""
        focusedJson = self.statsJson[ix]["splits"][0]["stat"]

        self.goals = focusedJson["goals"]
        self.assists = focusedJson["assists"]
        self.points = focusedJson["points"]
        self.shots = focusedJson["shots"]
        self.blockedShots = focusedJson["blocked"]
        self.shotPct = focusedJson["shotPct"]
        self.penaltyMinutes = focusedJson["pim"]
        self.gamesPlayed = focusedJson["games"]

        self.timeOnIce = focusedJson["timeOnIce"]
        self.timeOnIcePerGame = focusedJson["timeOnIcePerGame"]
        self.evenStrengthTimeOnIcePerGame = focusedJson["evenTimeOnIcePerGame"]
        if "shortHandedTimeOnIcePerGame" in focusedJson:
            self.shorthandedTimeOnIcePerGame = focusedJson["shortHandedTimeOnIcePerGame"]

        self.ppGoals = focusedJson["powerPlayGoals"]
        self.ppPoints = focusedJson["powerPlayPoints"]
        self.ppAssists = self.ppGoals - self.ppPoints
        self.ppTimeOnIce = focusedJson["powerPlayTimeOnIce"]
        self.ppTimeOnIcePerGame = focusedJson["powerPlayTimeOnIcePerGame"]
        self.gameWinningGoals = focusedJson["gameWinningGoals"]
        self.otGoals = focusedJson["overTimeGoals"]

        self.shorthandedGoals = focusedJson["shortHandedGoals"]
        self.shorthandedPoints = focusedJson["shortHandedPoints"]
        self.shorthandedAssists = self.shorthandedPoints - self.shorthandedGoals
        self.shorthandedTimeOnIce = focusedJson["shortHandedTimeOnIce"]

        self.faceoffPct = focusedJson["faceOffPct"]
        self.hits = focusedJson["hits"]
        self._plusMinus = focusedJson["plusMinus"]
        self.shifts = focusedJson["shifts"]

    #regularSeasonStatRankings
    def rankingsInit(self, ix):
        """Initialize Player Stat rankings among the league"""
        focusedJson = self.statsJson[ix]["splits"][0]["stat"]

        self.rankPpGoals = focusedJson["rankPowerPlayGoals"]
        self.rankBlockedShots = focusedJson["rankBlockedShots"]
        self.rankAssists = focusedJson["rankAssists"]
        self.rankShotPct = focusedJson["rankShotPct"]
        self.rankGoals = focusedJson["rankGoals"]
        self.rankHits = focusedJson["rankHits"]
        self.rankPenaltyMinutes = focusedJson["rankPenaltyMinutes"]
        self.rankShorthandedGoals = focusedJson["rankShortHandedGoals"]
        self.rankShots = focusedJson["rankShots"]
        self.rankPoints = focusedJson["rankPoints"]
        self.rankOtGoals = focusedJson["rankOvertimeGoals"]
        self.rankGamesPlayed = focusedJson["rankGamesPlayed"]
        self._rankPlusMinus = focusedJson["rankPlusMinus"]

    #goalsByGameSituation
    def goalSituationInit(self, ix):
        """Store goals scored in different situations"""
        self.goalsBySituation = self.statsJson[ix]["splits"][0]["stat"]

    #gameLog
    def gameLogInit(self, ix):
        """Populate stats per game"""
        focusedJson = self.statsJson[ix]["splits"]
        for game in focusedJson:
            gameLog = {
                "id": game["game"]["gamePk"],
                "win": game["isWin"],
                "OT": game["isOT"],
                "home": game["isHome"],
                "versus": game["opponent"]["name"],
                "versusId": game["opponent"]["id"],
                "team": game["team"]["name"],
                "teamId": game["team"]["id"],
            }
            gameLog.update(game["stat"])
            self.gameLog[game["game"]["gamePk"]] = gameLog

    #homeAndAway, winLoss, byMonth, vsDivision, vsConference, vsTeam
    def splitsInit(self, ix):
        """Set comparative stats for a player in a season"""
        name = self.statsJson[ix]["type"]["displayName"]
        focusedJson = self.statsJson[ix]["splits"]
        splits = {}
        if name == "homeAndAway":
            if focusedJson[0]["isHome"]:
                splits["home"] = focusedJson[0]
                splits["away"] = focusedJson[1]
            else:
                splits["home"] = focusedJson[1]
                splits["away"] = focusedJson[0]
        elif name == "vsDivision":
            for div in focusedJson:
                divisionLog = {
                    "division": div["opponentDivision"]["name"],
                    "divisionId": div["opponentDivision"]["id"],
                    "conference": div["opponentConference"]["name"],
                    "conferenceId": div["opponentConference"]["id"],
                }
                divisionLog.update(div["stat"])
                splits[div["opponentDivision"]["name"]] = divisionLog
        elif name == "vsConference":
            for conf in focusedJson:
                conferenceLog = conf["stat"]
                conferenceLog["conference"] = conf["opponentConference"]["name"]
                conferenceLog["conferenceId"] = conf["opponentConference"]["id"]
                splits[conf["opponentConference"]["name"]] = conferenceLog
        elif name == "vsTeam":
            for team in focusedJson:
                teamLog = team["stat"]
                teamLog["team"] = team["opponent"]["name"]
                teamLog["teamId"] = team["opponent"]["id"]
                teamLog["division"] = team["opponentDivision"]["name"]
                teamLog["divisionId"] = team["opponentDivision"]["id"]
                teamLog["conference"] = team["opponentConference"]["name"]
                teamLog["conferenceId"] = team["opponentConference"]["id"]
                splits[team["opponentConference"]["name"]] = teamLog
        elif name == "winLoss":
            for situation in focusedJson:
                statLog = situation["stat"]
                if situation["isWin"]:
                    splits["win"] = statLog
                elif situation["isOT"]:
                    splits["loss"] = statLog
                else: #Both false
                    splits["otLoss"] = statLog
        elif name == "byMonth":
            for month in focusedJson:
                splits[month["month"]] = month["stat"]
        else:
            print("Unknown stats identifier {}. Please report to code author!".format(name))
        self.splits[name] = splits

    #regularSeasonStatRankings
    def goalieRankingsInit(self, ix):
        """Set ranks for goalie stats in a season"""
        focusedJson = self.statsJson[ix]["splits"][0]["stat"]

        self.rankShotsAgainst = focusedJson["shotsAgainst"]
        self.rankOt = focusedJson["ot"]
        self.rankPenaltyMinutes = focusedJson["penaltyMinutes"]
        self.rankTimeOnIce = focusedJson["timeOnIce"]
        self.rankShutouts = focusedJson["shutOuts"]
        self.rankSaves = focusedJson["saves"]
        self.rankLosses = focusedJson["losses"]
        self.rankGoalsAgainst = focusedJson["goalsAgainst"]
        self.rankSavePercentage = focusedJson["savePercentage"]
        self.rankGames = focusedJson["games"]
        self.rankGoalsAgainstAverage = focusedJson["goalsAgainstAverage"]
        self.rankWins = focusedJson["wins"]

    #statsSingleSeason, onPaceRegularSeason
    def singleSplitGoalieInit(self, ix):
        """Set goalie values for gross, projected, or past player stats"""
        focusedJson = self.statsJson[ix]["splits"][0]["stat"]

        self.timeOnIce = focusedJson["timeOnIce"]
        self.ot = focusedJson["ot"]
        self.shutouts = focusedJson["shutouts"]
        self.ties = focusedJson["ties"]
        self.wins = focusedJson["wins"]
        self.losses = focusedJson["losses"]
        self.saves = focusedJson["saves"]
        self.ppSaves = focusedJson["powerPlaySaves"]
        self.shSaves = focusedJson["shortHandedSaves"]
        self.evenSaves = focusedJson["evenSaves"]
        self.shShots = focusedJson["shortHandedShots"]
        self.evenShots = focusedJson["evenShots"]
        self.ppShots = focusedJson["powerPlayShots"]
        self.savePct = focusedJson["savePercentage"]
        self.goalsAgainstAverage = focusedJson["goalAgainstAverage"]
        self.games = focusedJson["games"]
        self.gamesStarted = focusedJson["gamesStarted"]
        self.shotsAgainst = focusedJson["shotsAgainst"]
        self.goalsAgainst = focusedJson["goalsAgainst"]
        self.timeOnIcePerGame = focusedJson["timeOnIcePerGame"]
        self.ppSavePct = focusedJson["powerPlaySavePercentage"]
        self.shSavePct = focusedJson["shortHandedSavePercentage"]
        self.evenSavePct = focusedJson["evenStrengthSavePercentage"]
